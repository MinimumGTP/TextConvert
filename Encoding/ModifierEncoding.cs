﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextConvert.Encoding
{
    public class ModifierEncoding
    {
        private static Dictionary<char, char> EncodeTable = new Dictionary<char, char>();
        private static Dictionary<char, char> DecodeTable = new Dictionary<char, char>();

        public static void Initialize()
        {
            AddCharacter('h', '\u02b0');
            AddCharacter('j', '\u02b2');
            AddCharacter('r', '\u02b3');
            AddCharacter('w', '\u02b7');
            AddCharacter('y', '\u02b8');
            AddCharacter('\'', '\u02bc');
            AddCharacter('l', '\u02e1');
            AddCharacter('s', '\u02e2');
            AddCharacter('x', '\u02e3');
            AddCharacter('\"', '\u02ee');
            AddCharacter('A', '\u1d2c');
            AddCharacter('B', '\u1d2e');
            AddCharacter('D', '\u1d30');
            AddCharacter('E', '\u1d31');
            AddCharacter('G', '\u1d33');
            AddCharacter('H', '\u1d34');
            AddCharacter('I', '\u1d35');
            AddCharacter('J', '\u1d36');
            AddCharacter('K', '\u1d37');
            AddCharacter('L', '\u1d38');
            AddCharacter('M', '\u1d39');
            AddCharacter('N', '\u1d3a');
            AddCharacter('O', '\u1d3c');
            AddCharacter('P', '\u1d3e');
            AddCharacter('R', '\u1d3f');
            AddCharacter('T', '\u1d40');
            AddCharacter('U', '\u1d41');
            AddCharacter('W', '\u1d42');
            AddCharacter('a', '\u1d43');
            AddCharacter('b', '\u1d47');
            AddCharacter('d', '\u1d48');
            AddCharacter('e', '\u1d49');
            AddCharacter('g', '\u1d4d');
            AddCharacter('k', '\u1d4f');
            AddCharacter('m', '\u1d50');
            AddCharacter('o', '\u1d52');
            AddCharacter('p', '\u1d56');
            AddCharacter('t', '\u1d57');
            AddCharacter('u', '\u1d58');
            AddCharacter('v', '\u1d5b');
            AddCharacter('c', '\u1d9c');
            AddCharacter('f', '\u1da0');
        }

        public static char Encode(char character)
        {
            char encodedChar = character;

            if (EncodeTable.ContainsKey(character))
            {
                encodedChar = EncodeTable[character];
            }

            return encodedChar;
        }

        public static String Encode(String text)
        {
            String encodedString = "";

            foreach (char character in text)
            {
                encodedString += Encode(character);
            }

            return encodedString;
        }

        private static void AddCharacter(char decoded, char encoded)
        {
            EncodeTable.Add(decoded, encoded);
            DecodeTable.Add(encoded, decoded);

            return;
        }
    }
}

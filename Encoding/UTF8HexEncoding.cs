﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextConvert.Encoding
{
    public class UTF8HexEncoding
    {
        public static String Encode(char character)
        {
            return String.Format(@"U+{0:x4}", (ushort) character);
        }

        public static String Encode(String text)
        {
            String encodedString = "";

            foreach (char character in text)
            {
                encodedString += Encode(character) + " ";
            }

            return encodedString;
        }
    }
}

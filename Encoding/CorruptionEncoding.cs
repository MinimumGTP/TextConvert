﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextConvert.Encoding
{
    public static class CorruptionEncoding
    {
        public static String Encode(char character)
        {
            return Encode(character, new Random().Next());
        }

        public static String Encode(char character, int seed)
        {
            Random rand = new Random(seed);
            String corruptedChar = "" + character;

            int numChars = 6 + rand.Next(16);

            for (int x = 0; x < numChars; x++)
            {
                char corruption = (char) (790 + rand.Next(76));

                corruptedChar += corruption;
            }

            return corruptedChar;
        }

        public static String Encode(String text)
        {
            String encodedString = "";
            Random rand = new Random();

            foreach (char character in text)
            {
                encodedString += Encode(character, rand.Next()) + " ";
            }

            return encodedString;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace TextConvert.Encoding
{
    public static class FullwidthEncoding
    {
        private static Dictionary<char, char> EncodeTable = new Dictionary<char, char>();
        private static Dictionary<char, char> DecodeTable = new Dictionary<char, char>();

        public static void Initialize()
        {
            AddCharacter('!', '！');
            AddCharacter('\"', '＂');
            AddCharacter('#', '＃');
            AddCharacter('$', '＄');
            AddCharacter('%', '％');
            AddCharacter('&', '＆');
            AddCharacter('\'', '＇');
            AddCharacter('(', '（');
            AddCharacter(')', '）');
            AddCharacter('*', '＊');
            AddCharacter('+', '＋');
            AddCharacter(',', '，');
            AddCharacter('-', '－');
            AddCharacter('.', '．');
            AddCharacter('/', '／');
            AddCharacter('0', '０');
            AddCharacter('1', '１');
            AddCharacter('2', '２');
            AddCharacter('3', '３');
            AddCharacter('4', '４');
            AddCharacter('5', '５');
            AddCharacter('6', '６');
            AddCharacter('7', '７');
            AddCharacter('8', '８');
            AddCharacter('9', '９');
            AddCharacter(':', '：');
            AddCharacter(';', '；');
            AddCharacter('<', '＜');
            AddCharacter('=', '＝');
            AddCharacter('>', '＞');
            AddCharacter('?', '？');
            AddCharacter('@', '＠');
            AddCharacter('A', 'Ａ');
            AddCharacter('B', 'Ｂ');
            AddCharacter('C', 'Ｃ');
            AddCharacter('D', 'Ｄ');
            AddCharacter('E', 'Ｅ');
            AddCharacter('F', 'Ｆ');
            AddCharacter('G', 'Ｇ');
            AddCharacter('H', 'Ｈ');
            AddCharacter('I', 'Ｉ');
            AddCharacter('J', 'Ｊ');
            AddCharacter('K', 'Ｋ');
            AddCharacter('L', 'Ｌ');
            AddCharacter('M', 'Ｍ');
            AddCharacter('N', 'Ｎ');
            AddCharacter('O', 'Ｏ');
            AddCharacter('P', 'Ｐ');
            AddCharacter('Q', 'Ｑ');
            AddCharacter('R', 'Ｒ');
            AddCharacter('S', 'Ｓ');
            AddCharacter('T', 'Ｔ');
            AddCharacter('U', 'Ｕ');
            AddCharacter('V', 'Ｖ');
            AddCharacter('W', 'Ｗ');
            AddCharacter('X', 'Ｘ');
            AddCharacter('Y', 'Ｙ');
            AddCharacter('Z', 'Ｚ');
            AddCharacter('[', '［');
            AddCharacter('\\', '＼');
            AddCharacter(']', '］');
            AddCharacter('^', '＾');
            AddCharacter('_', '＿');
            AddCharacter('`', '｀');
            AddCharacter('a', 'ａ');
            AddCharacter('b', 'ｂ');
            AddCharacter('c', 'ｃ');
            AddCharacter('d', 'ｄ');
            AddCharacter('e', 'ｅ');
            AddCharacter('f', 'ｆ');
            AddCharacter('g', 'ｇ');
            AddCharacter('h', 'ｈ');
            AddCharacter('i', 'ｉ');
            AddCharacter('j', 'ｊ');
            AddCharacter('k', 'ｋ');
            AddCharacter('l', 'ｌ');
            AddCharacter('m', 'ｍ');
            AddCharacter('n', 'ｎ');
            AddCharacter('o', 'ｏ');
            AddCharacter('p', 'ｐ');
            AddCharacter('q', 'ｑ');
            AddCharacter('r', 'ｒ');
            AddCharacter('s', 'ｓ');
            AddCharacter('t', 'ｔ');
            AddCharacter('u', 'ｕ');
            AddCharacter('v', 'ｖ');
            AddCharacter('w', 'ｗ');
            AddCharacter('x', 'ｘ');
            AddCharacter('y', 'ｙ');
            AddCharacter('z', 'ｚ');
            AddCharacter('{', '｛');
            AddCharacter('|', '｜');
            AddCharacter('}', '｝');
            AddCharacter('~', '～');
            //AddCharacter('', '');

            return;
        }

        public static char Encode(char character)
        {
            char encodedChar = character;

            if (EncodeTable.ContainsKey(character))
            {
                encodedChar = EncodeTable[character];
            }

            return encodedChar;
        }

        public static String Encode(String text)
        {
            String encodedString = "";

            foreach (char character in text)
            {
                encodedString += Encode(character);
            }

            return encodedString;
        }

        private static void AddCharacter(char decoded, char encoded)
        {
            EncodeTable.Add(decoded, encoded);
            DecodeTable.Add(encoded, decoded);

            return;
        }
    }
}

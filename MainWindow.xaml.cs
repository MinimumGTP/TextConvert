﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TextConvert.Encoding;

namespace TextConvert
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            FullwidthEncoding.Initialize();
            ModifierEncoding.Initialize();
        }

        private void EncodeText()
        {
            if (EncodeTypeBox.SelectedItem == FullwidthItem)
            {
                if (OutputBox != null)
                    OutputBox.Text = FullwidthEncoding.Encode(InputBox.Text);
            }
            else if (EncodeTypeBox.SelectedItem == UTF8HexItem)
            {
                if(OutputBox != null)
                    OutputBox.Text = UTF8HexEncoding.Encode(InputBox.Text);
            }
            else if (EncodeTypeBox.SelectedItem == ModifierItem)
            {
                if (OutputBox != null)
                    OutputBox.Text = ModifierEncoding.Encode(InputBox.Text);
            }
            else if (EncodeTypeBox.SelectedItem == CorruptionItem)
            {
                if (OutputBox != null)
                    OutputBox.Text = CorruptionEncoding.Encode(InputBox.Text);
            }

            return;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            EncodeText();
        }

        private void InputBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            EncodeText();
        }
    }
}
